#include "gazebo_riders_plugin.hh"

using namespace gazebo;
GZ_REGISTER_SYSTEM_PLUGIN(GazeboRidersPlugin)


// Constructor
GazeboRidersPlugin::GazeboRidersPlugin() : SystemPlugin()
{

}

// Deconstructor
GazeboRidersPlugin::~GazeboRidersPlugin()
{
    this->event.reset();
}

// Runs once on initialization
void GazeboRidersPlugin::Load(int /*_argc*/, char ** /*_argv*/)
{	
	this->ready_flag = false;
    
    this->updateConnection = event::Events::ConnectWorldUpdateBegin(
        std::bind(&GazeboRidersPlugin::OnUpdate, this));

	this->event = gazebo::event::Events::ConnectWorldCreated(boost::bind(&GazeboRidersPlugin::GetWorld,this,_1));


	if (!ros::isInitialized())
	{
		int argc = 0;
		char **argv = NULL;
		ros::init(argc, argv, "gazebo_riders_plugin",
		    ros::init_options::NoSigintHandler);
	}

	this->rosNode.reset(new ros::NodeHandle("gazebo_riders_plugin"));

	this->gazebo_state_pub = this->rosNode->advertise<std_msgs::String>("gazebo_state", 1000);
	this->model_state_pub = this->rosNode->advertise<std_msgs::String>("gazebo_model_state", 1000);

}

void GazeboRidersPlugin::Init()
{
	GazeboRidersPlugin::PublishString(this->gazebo_state_pub, "loading");
}

// Update Callback
void GazeboRidersPlugin::OnUpdate()
{
	if(!this->ready_flag) {
		GazeboRidersPlugin::PublishString(this->gazebo_state_pub, "ready");	
		this->ready_flag = true;
	}
	// Multitasking
    std::lock_guard<std::mutex> lock(this->mutex);
    this->model_names = GazeboRidersPlugin::WatchModels();
}

void GazeboRidersPlugin::GetWorld(std::string world_name)
{		
    this->world = gazebo::physics::get_world(world_name);
    this->world->SetPaused(true); 	
}

void GazeboRidersPlugin::PublishString(ros::Publisher pub, std::string data)
{
    std_msgs::String msg;
	msg.data = data;
	pub.publish(msg);
}

std::vector<std::string> GazeboRidersPlugin::GetModelNames()
{
	std::vector<std::string> model_names;
	this->models = this->world->Models();

	for (std::vector<physics::ModelPtr>::iterator model_i = this->models.begin();
	model_i != this->models.end(); ++model_i) {
		model_names.push_back((*model_i)->GetName());
	}
	return model_names;
}

std::vector<std::string> GazeboRidersPlugin::WatchModels() {
	std::vector<std::string> new_model_names;
	std::vector<std::string> temp_model_names;
	new_model_names = GazeboRidersPlugin::GetModelNames();

	std::sort(new_model_names.begin(), new_model_names.end());
	temp_model_names = new_model_names;

	if (!std::equal(this->model_names.begin(), this->model_names.end(), 
		new_model_names.begin(), new_model_names.end())) 
	{
		for(std::vector<std::string>::iterator name_i = this->model_names.begin();
			name_i != this->model_names.end(); ++name_i) {

			if (!(std::binary_search(new_model_names.begin(), new_model_names.end(), (*name_i)))) {
				GazeboRidersPlugin::PublishString(this->model_state_pub, "{\"despawned\" : \"" + (*name_i) + "\"}");

			}

			temp_model_names.erase(std::remove(temp_model_names.begin(), temp_model_names.end(),
			 (*name_i)), temp_model_names.end());	
		}
		for(std::vector<std::string>::iterator temp_i = temp_model_names.begin();
			temp_i != temp_model_names.end(); ++temp_i) {

			GazeboRidersPlugin::PublishString(this->model_state_pub, "{\"spawned\" : \"" + (*temp_i) + "\"}");
		}
	}
	return new_model_names;
}