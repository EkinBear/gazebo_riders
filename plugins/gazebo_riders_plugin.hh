#ifndef GAZEBO_PLUGINS_TrafficLightModelPlugin_HH_
#define GAZEBO_PLUGINS_TrafficLightModelPlugin_HH_

#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"
#include "gazebo/physics/physics.hh"
#include <gazebo/rendering/Visual.hh>
#include <ignition/transport/Node.hh>
#include <gazebo/transport/Node.hh>
#include <mutex>

#include "ros/ros.h"
#include "std_msgs/String.h"
namespace gazebo
{

  class GAZEBO_VISIBLE GazeboRidersPlugin : public SystemPlugin
  {
    // Constructor
    public: GazeboRidersPlugin();
    // Deconstructor
    public: virtual ~GazeboRidersPlugin();
    // Load Function
    // Called after the plugin has been constructed.
    public: virtual void Load(int /*_argc*/, char ** /*_argv*/);
    // Called once after Load
    public: virtual void Init();
    // Update Function
    // Updates on Every World Update
    private: virtual void OnUpdate();

    private: virtual void GetWorld(std::string world_name);

    private: virtual std::vector<std::string> GetModelNames();

    private: virtual std::vector<std::string> WatchModels();

    private: virtual void PublishString(ros::Publisher pub, std::string data); 
    ////////////////////////////////////////////////////////////////////////////
    // Connection to World Update events.
    private: event::ConnectionPtr updateConnection;
    // Node Used for Communication.
    private: std::mutex mutex;

    private: physics::WorldPtr world;

    private: gazebo::event::ConnectionPtr event;

    public: std::unique_ptr<ros::NodeHandle> rosNode;

    private: physics::Model_V models;

    private: std::vector<std::string> model_names;
    //ROS publisher    
    private: ros::Publisher gazebo_state_pub;

    private: ros::Publisher model_state_pub;

    private: bool ready_flag;
  };
}
#endif