# GAZEBO RIDERS PLUGIN

Gazebo Riders Plugin publishes data through ROS topics in order to inform about gazebo state and spawned models. This plugin starts simulation in paused state so when all the necessary components are loaded simulation can be started by hand.



## Usage

Since the plugin is a System Plugin, it can be used at launching gazebo like this:

```bash
gazebo -s ${PATH_TO_PLUGIN}/libgazebo_riders_plugin.so
```

**For this plugin to work roscore must be active.**

# Example

Gazebo state can be read through /gazebo_riders_plugin/gazebo_state ROS topic. When gazebo is ready topic will return "ready".

An event is created when a model is spawned or despawned and publishes model's name to  /gazebo_riders_plugin/gazebo_model_state.

**Python code implementation can be found in ${PACKAGE_DIR}/scripts/gazebo_riders.py**

