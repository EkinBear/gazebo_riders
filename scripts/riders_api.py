import logging
import os

import time
import riders
from riders.rest import ApiException
import json
from pprint import pprint

logger = logging.getLogger(__name__)

API_KEY = os.environ.get('RIDERS_AUTH_TOKEN', None)
RIDERS_HOST = str(os.environ.get('RIDERS_HOST', None))

CHALLENGE_ID = os.environ.get('CHALLENGE_ID', None)
CHALLENGE_GAME_ID = os.environ.get('CHALLENGE_GAME_ID', None)
CHALLENGE_GAME_PLAY_ID = os.environ.get('CHALLENGE_GAME_PLAY_ID', None)

#RIDERS API Python Client Service
class RidersApiService():

    def __init__(self):

        self.configuration = riders.Configuration()
        self.configuration.host = RIDERS_HOST
        self.configuration.api_key['Authorization'] = API_KEY
        self.configuration.api_key_prefix['Authorization'] = 'Token'

        self.container_api = riders.ContainerApi(riders.ApiClient(self.configuration))
        self.challenge_api = riders.ChallengeApi(riders.ApiClient(self.configuration))
        
    def challenge_game_plays_event_create(self, game_play, info, simulation_time):
        challenge_pk = CHALLENGE_ID
        game_play_pk = CHALLENGE_GAME_ID

        data = riders.ChallengeGamePlayEvent(game_play, info, simulation_time)

        try:
            api_response = self.challenge_api.challenge_game_plays_event_create(challenge_pk, game_play_pk, data)
            pprint(api_response)
        except ApiException as e:
            print("Exception when calling ChallengeApi->challenge_game_plays_event_create: %s\n" % e)

