#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import riders_api

class GazeboRidersService():

    # Time limit in sec(min * sec)
    time_limit = 5*60

    def __init__(self):
        rospy.init_node('gazebo_riders', anonymous=True)

        # Subscribes to topics
        rospy.Subscriber("/gazebo_riders_plugin/gazebo_state", String, self.on_state)
        rospy.Subscriber("/gazebo_riders_plugin/gazebo_model_state", String, self.on_model_state)


        # Tries to initiliaze services
        # Services must be active otherwise code won't work
        while True:
            try:
                if '/gazebo/unpause_physics' in rosservice.get_service_list():
                    self.unpause = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)
                    break
            except:
                pass


    # gazebo_riders_plugin/gazebo_state callback
    # Enters fucntion when an event is created.
    def on_state(self, msg):
        print(msg.data)

    # gazebo_riders_plugin/gazebo_model_state callback
    # Enters fucntion when an event is created.
    def on_model_state(self, msg):
        print(msg.data)

    #Infinite loop
    def loop(self):
        rospy.spin()

    # Unpauses simulator
    def unpause_sim(self):
        self.unpause()



if __name__ == '__main__':
    gazebo_riders = GazeboRidersService()
    gazebo_riders.loop()
    gazebo_riders.unpause()